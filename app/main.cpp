// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVisualProgrammingMainWindow.h"

#include <dtkLog>
#include <dtkCore>
#include <dtkWidgets>
#include <dtkComposer>

#include <QtWidgets>

int main(int argc, char **argv)
{
#if defined (Q_OS_UNIX)
    setenv("QT_PLUGIN_PATH", "", 1);
#endif

    dtkApplication *application = dtkApplication::create(argc, argv);
    application->setApplicationName("dtkVisualProgramming");
    application->setOrganizationName("inria");
    application->setOrganizationDomain("fr");
    application->setApplicationVersion("1.1.3");

    QCommandLineParser *parser = application->parser();
    parser->setApplicationDescription("dtk visual programming.");

    application->initialize();

    QCommandLineOption verboseOption("verbose", QCoreApplication::translate("main", "verbose plugin initialization"));

    if (parser->isSet(verboseOption)) {
        dtkComposer::extension::pluginManager().setVerboseLoading(true);
    }
    dtkComposer::node::initialize();
    dtkComposer::extension::initialize();

    dtkVisualProgrammingMainWindow mainwindow;
    mainwindow.show();
    mainwindow.raise();

    int status = application->exec();

    return status;
}

//
// main.cpp ends here
